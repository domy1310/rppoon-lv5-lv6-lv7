﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    class StringLengthChecker : StringChecker
    {
        private const int DEFAULT_STRING_LENGTH = 8;
        public int StringLength { get; set; }

        public StringLengthChecker()
        {
            this.StringLength = DEFAULT_STRING_LENGTH;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Length >= this.StringLength)
            {
                return true;
            }
            return false;
        }
    }
}
