﻿using System;
using System.Collections.Generic;

namespace LV_6
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ===== Zadatak 1 =====*/
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("RPPOON", "Razvoj programske podrške objektno orijentiranim načelima"));
            notebook.AddNote(new Note("OAU", "Osnove automatskog upravljanja"));
            notebook.AddNote(new Note("KV", "Komunikacijske vještine"));

            Iterator iterator = (Iterator)notebook.GetIterator();

            for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {
                note.Show();
            }

            /* ===== Zadatak 2 =====*/
            Console.WriteLine("\n");
            Box box = new Box();
            box.AddProduct(new Product("Mlijeko", 3.99));
            box.AddProduct(new Product("Sećer", 2.99));
            box.AddProduct(new Product("Sol", 3.5));
            box.AddProduct(new Product("Jaja", 9.99));

            BoxIterator boxIterator = (BoxIterator)box.GetIterator();

            for(Product product = boxIterator.First(); boxIterator.IsDone == false; product = boxIterator.Next())
            {
                Console.WriteLine(product.ToString());
            }

            /* ===== Zadatak 3 =====*/
            Console.WriteLine("\n");
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("RPPOON", "Predavanje", DateTime.Now.AddMinutes(45));

            Console.WriteLine(toDoItem.ToString());
            careTaker.StoreMemento(toDoItem.StoreState());

            toDoItem.ChangeTask("Auditorne vježbe");
            Console.WriteLine(toDoItem.ToString());
            careTaker.StoreMemento(toDoItem.StoreState());

            toDoItem.Rename("OAU");
            toDoItem.ChangeTask("Laboratorijske vježbe");
            toDoItem.ChangeTimeDuo(DateTime.Now.AddMinutes(135));
            Console.WriteLine(toDoItem.ToString());
            careTaker.StoreMemento(toDoItem.StoreState());

            Console.WriteLine("\nUčitavanje...");
            while (!careTaker.IsEmpty())
            {
                toDoItem.RestoreState(careTaker.GetLastMemento());
                Console.WriteLine(toDoItem.ToString());
            }

            /* ===== Zadatak 4 =====*/
            Console.WriteLine("\n");
            BankAccount bankAccount = new BankAccount("Dominik", "Vukovarska 111", 15000);

            Console.WriteLine(bankAccount.ToString());
            Save save = bankAccount.Store();

            bankAccount.UpdateBalance(10000);
            Console.WriteLine(bankAccount.ToString());

            bankAccount.Restore(save);
            Console.WriteLine(bankAccount.ToString());

            /* ===== Zadatak 5 =====*/
            Console.WriteLine("\n");
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);
            logger.Log("Upozorenje", MessageType.WARNING);
            logger.Log("Greška", MessageType.ERROR);
            logger.Log("Info", MessageType.INFO);

            /* ===== Zadatak 6 =====*/
            StringChecker stringDigitChecker = new StringDigitChecker();
            StringChecker stringUpperChecker = new StringUpperCaseChecker();
            StringChecker stringLowerChecker = new StringLowerCaseChecker();
            StringChecker stringLengthChecker = new StringLengthChecker();

            stringDigitChecker.SetNext(stringUpperChecker);
            stringUpperChecker.SetNext(stringLowerChecker);
            stringLowerChecker.SetNext(stringLengthChecker);
           

            List<string> stringToCheck = new List<string>();
            stringToCheck.Add("Dominik Tkalcec");
            stringToCheck.Add("dominik tkalcec");
            stringToCheck.Add("domi");
            stringToCheck.Add("domi1997");
            stringToCheck.Add("Domi1997");

            foreach (string stringCheck in stringToCheck)
            {
                Console.Write("Provjera riječi: " + stringCheck + " => ");
                if (stringDigitChecker.Check(stringCheck))
                    Console.Write("True");
                else
                    Console.Write("False");
                Console.WriteLine();
            }

            /* ===== Zadatak 7 =====*/
            Console.WriteLine("\n");
            PasswordValidator passwordValidator = new PasswordValidator(new StringDigitChecker());
            passwordValidator.AddChecker(new StringLengthChecker());
            passwordValidator.AddChecker(new StringUpperCaseChecker());
            passwordValidator.AddChecker(new StringLowerCaseChecker());
            
            foreach (string stringCheck in stringToCheck)
            {
                Console.Write("Provjera riječi: " + stringCheck + " => ");
                if (passwordValidator.Check(stringCheck))
                    Console.Write("True");
                else
                    Console.Write("False");
                Console.WriteLine();
            }

        }
    }
}
