﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    class FileLogger : AbstractLogger
    {
        private string FilePath { get; }
        public FileLogger(MessageType messageType, string filePath) : base(messageType) 
        {
            this.FilePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.FilePath, true))
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(type + ": " + DateTime.Now + "\n");
                stringBuilder.Append(new string('=', message.Length) + "\n");
                stringBuilder.Append(message + "\n");
                stringBuilder.Append(new string('=', message.Length) + "\n");
                writer.WriteLine(stringBuilder.ToString());
            }
        }
    }
}
