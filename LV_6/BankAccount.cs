﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    class BankAccount
    {
        private string ownerName;
        private string ownerAddress;
        private decimal balance;

        public BankAccount(string ownerName, string ownerAddress, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAddress = ownerAddress;
            this.balance = balance;
        }

        public void ChangeOwnerAddress(string address)
        {
            this.ownerAddress = address;
        }

        public void UpdateBalance(decimal amount)
        {
            this.balance = amount;
        }

        public Save Store()
        {
            return new Save(this.ownerName, this.ownerAddress, this.balance);
        }

        public void Restore(Save save)
        {
            this.ownerName = save.OwnerName;
            this.ownerAddress = save.OwnerAddress;
            this.balance = save.Balance;
        }

        public string OwnerName
        {
            get { return this.ownerName; }
        }

        public string OwnerAddress
        {
            get { return this.ownerAddress; }
        }

        public decimal Balance
        {
            get { return this.balance; }
        }

        public override string ToString()
        {
            return "Account owner: " + ownerName + ", owner address: " + ownerAddress + " ==> Total balance: " + balance;
        }
    }
}
