﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    class PasswordValidator
    {
        private StringChecker firstStringChecker;
        private StringChecker lastStringChecker;
        public PasswordValidator(StringChecker stringChecker)
        {
            this.firstStringChecker = stringChecker;
            this.lastStringChecker = stringChecker;
        }

        public void AddChecker(StringChecker stringChecker)
        {
            this.lastStringChecker.SetNext(stringChecker);
            this.lastStringChecker = stringChecker;
        }

        public bool Check(string stringToCheck)
        {
            return this.firstStringChecker.Check(stringToCheck);
        }
    }
}
