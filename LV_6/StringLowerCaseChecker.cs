﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV_6
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Any(char.IsLower);
        }
    }
}
