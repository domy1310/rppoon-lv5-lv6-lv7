﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Text;

namespace LV_6
{
    class Box : IBoxCollection
    {
        private List<Product> products;

        public Box()
        {
            this.products = new List<Product>();
        }

        public Box(Product product)
        {
            this.products = new List<Product>(products.ToArray());
        }

        public void AddProduct(Product product)
        {
            this.products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            this.products.Remove(product);
        }

        public int Count
        {  
            get { return this.products.Count; } 
        }

        public Product this[int index]
        {
            get { return this.products[index]; }
        }

        public IBoxIterator GetIterator()
        {
            return new BoxIterator(this);
        }
    }
}
