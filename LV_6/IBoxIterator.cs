﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    interface IBoxIterator
    {
        Product First();
        Product Next();
        bool IsDone { get; }
        Product Current { get; }
    }
}
