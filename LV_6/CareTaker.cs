﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6
{
    class CareTaker
    {
        Stack<Memento> mementos;

        public CareTaker()
        {
            this.mementos = new Stack<Memento>();
        }

        public void StoreMemento(Memento memento)
        {
            this.mementos.Push(memento);
        }

        public Memento GetLastMemento()
        {
            return this.mementos.Pop();
        }

        public bool IsEmpty()
        {
            return this.mementos.Count <= 0;
        }

    }
}
