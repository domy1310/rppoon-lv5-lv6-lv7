﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class ShippingService
    {
        private double pricePerUnit;

        public ShippingService(double pricePerUnit)
        {
            this.pricePerUnit = pricePerUnit;
        }

        public double CalculateShippingPrice(double packageWeight)
        {
            return pricePerUnit * packageWeight;
        }
    }
}
