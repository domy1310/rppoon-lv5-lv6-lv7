﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    interface IDataset
    {
        IReadOnlyCollection<List<string>> GetData();
    }
}
