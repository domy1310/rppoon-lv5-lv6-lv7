﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class GroupNote : Note
    {
        private List<string> members;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            members = new List<string>();
        }

        public void AddMember(string name)
        {
            if (!members.Contains(name))
                members.Add(name);
        }

        public void RemoveMember(string name)
        {
            members.Remove(name);
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("GROUP: ");
            string framedMessage = this.GetFramedMessage();

            foreach (string member in members)
            {
                Console.WriteLine(member);
            }
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
    }
}
