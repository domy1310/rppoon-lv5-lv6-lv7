﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class ModernTheme : ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
        }
        
        public string GetHeader(int width)
        {
            return new string('-', width);
        }

        public string GetFooter(int width)
        {
            return new string('=', width);
        }
    }
}
