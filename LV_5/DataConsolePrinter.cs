﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace LV_5
{
    class DataConsolePrinter
    {
        public void print(IDataset dataset)
        {
            IReadOnlyCollection<List<string>> loadedData = dataset.GetData();
            if (loadedData != null)
            {
                foreach (List<string> line in loadedData)
                {
                    foreach (string item in line)
                    {
                        Console.Write(item + ", ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
