﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace LV_5
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ===== Zadatak 1 =====*/
            List<IShipable> order = new List<IShipable>();

            Product milk = new Product("Cow milk", 4.99, 1000);
            Product sugar = new Product("White sugar", 3.99, 1000);
            Box kinderBox = new Box("Kinder pack");
            kinderBox.Add(new Product("Bueno", 2.99, 30));
            kinderBox.Add(new Product("Pingui", 3.99, 20));

            order.Add(milk);
            order.Add(sugar);
            order.Add(kinderBox);

            double totalPrice = 0, totalWeight = 0;
            foreach(IShipable product in order)
            {
                totalPrice += product.Price;
                totalWeight += product.Weight;
                Console.WriteLine(product.Description());
            }
            Console.WriteLine("Total price: " + totalPrice + "kn , Total weight: " + totalWeight + "g");

            /* ===== Zadatak 2 =====*/
            ShippingService shippingService = new ShippingService(0.01);
            Console.WriteLine("Shipping price for this package: " + shippingService.CalculateShippingPrice(totalWeight) + "kn");

            /* ===== Zadatak 3 =====*/
            DataConsolePrinter consolePrinter = new DataConsolePrinter();
            VirtualProxyDataset virtualProxy = new VirtualProxyDataset("zad_3.csv");
            Console.WriteLine("\nVirtual proxy test: ");
            consolePrinter.print(virtualProxy);

            User user = User.GenerateUser("Dominik");
            ProtectionProxyDataset protectionProxy = new ProtectionProxyDataset(user);
            Console.WriteLine("\nProtection proxy test for user ID 1: ");
            consolePrinter.print(protectionProxy);

            user = User.GenerateUser("Luka");
            Console.WriteLine("\nProtection proxy test for user ID 2: ");
            consolePrinter.print(protectionProxy = new ProtectionProxyDataset(user));

            /* ===== Zadatak 4 =====*/
            LoggerProxyDataset loggerProxy = new LoggerProxyDataset("zad_3.csv");
            Console.WriteLine("\nLogger proxy test: ");
            consolePrinter.print(loggerProxy);
            consolePrinter.print(loggerProxy);

            /* ===== Zadatak 5 =====*/
            ITheme theme = new LightTheme();
            ReminderNote reminderNote = new ReminderNote("Kolokvij", theme);
            reminderNote.Show();

            theme = new ModernTheme();
            ReminderNote reminderNote_1 = new ReminderNote("Predavanje", theme);
            reminderNote_1.Show();

            /* ===== Zadatak 6 =====*/
            GroupNote groupSR1 = new GroupNote("Predavanje iz kolegija RPPOON", new LightTheme());
            groupSR1.AddMember("Dominik");
            groupSR1.AddMember("Ivo");
            groupSR1.AddMember("Pero");
            groupSR1.Show();

            GroupNote groupSR2 = new GroupNote("Predavanje iz kolegija RPPOON", new ModernTheme());
            groupSR2.AddMember("Ivan");
            groupSR2.AddMember("Djuro");
            groupSR2.Show();

            /* ===== Zadatak 7 =====*/
            Notebook notebook = new Notebook(new LightTheme());
            notebook.AddNote(reminderNote);
            notebook.AddNote(reminderNote_1);
            notebook.AddNote(groupSR1);
            notebook.AddNote(groupSR2);
            notebook.Display();
            notebook.ChangeTheme(new ModernTheme());
            notebook.Display();
        }
    }
}
