﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class LoggerProxyDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;
        private ConsoleLogger logger;

        public LoggerProxyDataset(string filePath)
        {
            this.filePath = filePath;
            logger = ConsoleLogger.GetInstance();       
        }

        public IReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(this.filePath);
                logger.Log("Dataset created.", DateTime.Now);
            }
            IReadOnlyCollection<List<string>> loadedData = dataset.GetData();
            logger.Log("Data loaded.", DateTime.Now);
            return loadedData;
        }
    }
}
