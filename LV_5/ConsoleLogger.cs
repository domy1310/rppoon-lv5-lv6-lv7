﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class ConsoleLogger
    {
        private static ConsoleLogger logger;

        private ConsoleLogger()
        {
        }

        public static ConsoleLogger GetInstance()
        {
            if (logger == null)
            {
                logger = new ConsoleLogger();
            }
            return logger;
        }

        public void Log(string message, DateTime nowTime)
        {
            Console.WriteLine(message + "(" + nowTime + ")");
        }
    }
}
