﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_5
{
    class VirtualProxyDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;

        public VirtualProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public IReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(this.filePath);
            }
            return dataset.GetData();
        }
    }
}
