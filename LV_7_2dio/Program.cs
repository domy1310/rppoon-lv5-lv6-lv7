﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_7_2dio
{
    class Program
    {
        static void Main(string[] args)
        {
            /*===== Zadatak 3, 4 =====*/
            SystemDataProvider dataProvider = new SystemDataProvider();
            dataProvider.Attach(new ConsoleLogger());
            dataProvider.Attach(new FileLogger("LV7zad_3.txt"));

            while (true)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
