﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_7_2dio
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;

        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }

        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (IsDifferenceTenPercent(this.previousCPULoad, currentLoad))
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }

        public float GetAvailableRAM()
        {
            float currentRAMAvailable = this.AvailableRAM;
            if (IsDifferenceTenPercent(this.previousRAMAvailable, currentRAMAvailable))
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentRAMAvailable;
            return currentRAMAvailable;
        }

        public bool IsDifferenceTenPercent(float previous, float current)
        {
            double tenPercentOfCurrent = 0.1 * current;
            if (Math.Abs(previous - current) >= tenPercentOfCurrent)
                return true;
            else
                return false;
        }
    }
}
