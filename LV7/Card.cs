﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    class Card
    {
        private List<IItem> items;

        public Card()
        {
            items = new List<IItem>();
        }

        public void Add(IItem item)
        {
            this.items.Add(item);
        }

        public void Remove(IItem item)
        {
            this.items.Remove(item);
        }

        public double Accept(IVisitor visitor)
        {
            double totalRentPrice = 0;
            foreach (IItem item in this.items)
            {
                totalRentPrice += item.Accept(visitor);
            }
            return totalRentPrice;
        }
    }
}
