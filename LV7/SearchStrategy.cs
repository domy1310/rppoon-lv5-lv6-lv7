﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    interface SearchStrategy
    {
        public int Search(double[] array, double number);
    }
}
