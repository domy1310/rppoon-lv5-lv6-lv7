﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace LV7
{
    class LinearSearch : SearchStrategy
    {
        public int Search(double[] array, double number)
        {
            for (int i = 0; i < array.Length; i++) {
                if (array[i] == number)
                    return i;
            }
            return -1;
        }
    }
}
