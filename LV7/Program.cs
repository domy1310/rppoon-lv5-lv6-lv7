﻿using System;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            /*===== Zadatak 1 =====*/
            double[] array = { 3.4, 8.1, 5.2, 8.4, 2.1, 1.1, 0.1, 6.2, 2.5, 3.3, 2.5 };
            NumberSequence numberSequence = new NumberSequence(array);

            Console.WriteLine("Unsorted array: \n" + numberSequence);

            numberSequence.SetSortStrategy(new BubbleSort());
            numberSequence.Sort();
            Console.WriteLine("Array after Bubble sort: \n" + numberSequence);

            numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(new CombSort());
            numberSequence.Sort();
            Console.WriteLine("Array after Comb sort: \n" + numberSequence);

            numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(new SequentialSort());
            numberSequence.Sort();
            Console.WriteLine("Array after Sequential sort: \n" + numberSequence);

            /*===== Zadatak 2 =====*/
            numberSequence.SetSearchStrategy(new LinearSearch());
            int numberIndex = numberSequence.Search(2.5);
            if (numberIndex == -1)
                Console.WriteLine("Broj 2.5 se ne nalazi u polju.");
            else
                Console.WriteLine("Broj 2.5 se nalazi na " + numberIndex + " indeksu.");

            numberIndex = numberSequence.Search(2);
            if (numberIndex == -1)
                Console.WriteLine("Broj 2 se ne nalazi u polju.");
            else
                Console.WriteLine("Broj 2 se nalazi na " + numberIndex + " indeksu.");

            /*===== Zadatak 3, 4 =====*/
            // Zadaci 3 i 4 riješeni u projektu pod nazivom LV_7_2dio

            /*===== Zadatak 5 =====*/
            Console.WriteLine("\n");
            DVD dvd = new DVD("Fast and forious", DVDType.MOVIE, 100);
            VHS vhs = new VHS("Miroslav Ilić", 50);
            Book book = new Book("Vlak u snijegu", 40);

            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine(dvd.ToString());
            Console.WriteLine("-> Total price: " + dvd.Accept(buyVisitor));
          
            Console.WriteLine(vhs.ToString());
            Console.WriteLine("-> Total price: " + vhs.Accept(buyVisitor));

            Console.WriteLine(book.ToString());
            Console.WriteLine("-> Total price: " + book.Accept(buyVisitor));

            /*===== Zadatak 6 =====*/
            Console.WriteLine("\n");
            RentVisitor rentVisitor = new RentVisitor();

            Console.WriteLine(dvd.ToString());
            Console.WriteLine("-> Rent price: " + dvd.Accept(rentVisitor));

            Console.WriteLine(vhs.ToString());
            Console.WriteLine("-> Rent price: " + vhs.Accept(rentVisitor));

            Console.WriteLine(book.ToString());
            Console.WriteLine("-> Rent price: " + book.Accept(rentVisitor));

            DVD softwareDVD = new DVD("Windows 10", DVDType.SOFTWARE, 500);
            Console.WriteLine(softwareDVD.ToString());
            Console.WriteLine("-> Rent price: " + softwareDVD.Accept(rentVisitor));

            /*===== Zadatak 7 =====*/
            Console.WriteLine("\n");
            Card card = new Card();
            card.Add(dvd);
            card.Add(vhs);
            card.Add(book);
            card.Add(softwareDVD);

            Console.WriteLine("Total rent price with software: " + card.Accept(rentVisitor));

            card.Remove(softwareDVD);
            Console.WriteLine("Total rent price without software: " + card.Accept(rentVisitor));
        }
    }
}
