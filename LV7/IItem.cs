﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
